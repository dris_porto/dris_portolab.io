# DriS Website

[![Build Status](https://gitlab.com/dris_porto/badges/master/build.svg)](https://gitlab.com/dris_porto/dris_porto.gitlab.io/pipelines)
[![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)](https://gitlab.com/dris_porto/site/pipelines)

---

This repository keeps source files required to build __DriS Website__.


__DriS Website__ 
is built by [Jekyll](https://jekyllrb.com/) 
over the [GitLab pipeline](https://docs.gitlab.com/ee/ci/pipelines.html) 
infrastructure and hosted by [GitLab Pages](https://pages.gitlab.io).

Live __DriS Website__ can be found at https://dris_porto.gitlab.io/


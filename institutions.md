---
layout: page
title: Partners
permalink: /partners/
---

__Dris__ has been supported by:

* [FEUP](http://fe.up.pt/)
* [ISEP](http://isep.ipp.pt/)
* [INESC TEC](http://www.inesctec.pt/)
* [UMinho](http://www.uminho.pt/)
* [FMH.UL](http://fmh.utl.pt/)
* [ESSP](http://ess.ipp.pt/)

(c) 2017, Dris Dev Team

